# python-backend-quickstart-template
Cookie Cutter template to build a quick backend app in python

## Description
The purpose of this repo is to give a template to quickly setup an python backend app with :
- FastAPI facade
- Utility to collect data in the web 
- Database (Postgresql)

## Badges
TODO

## Installation
In order to generate a new project based on this template, you need:
- Python3.9
- [cookiecutter](https://github.com/cookiecutter/cookiecutter)

## Usage
First, edit the `cookiecutter.json`
Once done, just use the following command from the root directory
```shell
cookiecutter -o <path_to_output_dir> $(pwd)/ 
```
It will generate a project based on information used in `cookiecutter.json`
To enable git on your new fresh project, just do 
```shell
cd <path_to_output_dir>
git init
git add .
git ci -m "generate project from cokiecutter template"
```

## Support


## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

## Authors and acknowledgment


