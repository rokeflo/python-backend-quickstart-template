# {{ cookiecutter.project_name }} 

please find documentation at https://gitlab.io/{{ cookiecutter.project_slug }}/docs/

coverage report is available at https://gitlab.io/{{ cookiecutter.project_slug }}/coverage/

Lint report is available at https://gitlab.io/{{ cookiecutter.project_slug }}/lint/

Project is built and deployed to Pypi repository: https://gitlab.com/{{ cookiecutter.project_slug }}/-/packages/

A docker image is available at https://gitlab.com/{{ cookiecutter.project_slug }}/container_registry/
