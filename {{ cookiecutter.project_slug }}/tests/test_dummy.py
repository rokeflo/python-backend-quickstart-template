import logging

from {{ cookiecutter.project_slug }}.main import hello


def test_func(caplog):
    caplog.set_level(logging.INFO)
    hello()
    assert "Hello" in caplog.text
