"""
{{ cookiecutter.project_name }}
Module to configure {{ cookiecutter.project_name }} project
"""
import logging

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)


def hello() -> None:
    """
    Say hello
    """
    LOGGER.info("Hello world! Welcome to {{ cookiecutter.project_name }}")


if __name__ == "__main__":
    hello()
