Welcome to {{ cookiecutter.project_name }}'s documentation!
================================================

.. toctree::
   :maxdepth: 1
   :caption: Get started

   contribute.md

.. toctree::
   :maxdepth: 1
   :caption: Code documentation

   modules/modules.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
