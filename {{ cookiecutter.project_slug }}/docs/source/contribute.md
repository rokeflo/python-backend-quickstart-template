# Contributing

This section describe how to contribute to the {{ cookiecutter.project_name }} project.

#### Requirements

  - python3.9.7
  - make
  - virtualenv
  - docker
  - docker-compose

#### Initialize your environment

We use [pre-commit](https://pre-commit.com/) to lint and test each commit 
```bash
# install pre-commit on your host 
python -m pip install pre-commit
pre-commit install
# build virtualenv through make 
make install
# if you wan to play with local virtualenv
```

## Launch tests:

```bash
make test
```

## Lint project:

```bash
make lint
```

## Reformat code:

```bash
make format
```

## Build documentation

```bash
make doc
```
